//
//  DayTableViewCell.swift
//  DailyNoteIOS
//
//  Created by Mikołaj Pęcak on 08.10.2017.
//  Copyright © 2017 Mikołaj Pęcak. All rights reserved.
//

import UIKit
import FirebaseDatabase

class DayTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    var day = 0
    var month = 0
    var year = 0
    var user = ""
    var database: DatabaseReference!
    var delegate: DayTableCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func saveNote(_ sender: UIButton) {
        textView.resignFirstResponder()
        database = Database.database().reference()
        database.child("\(user)/\(year)/\(month)/\(day)").setValue(["note": textView.text])
        delegate?.finishSavingData(day: day, month: month, year: year)
    }
}

protocol DayTableCellDelegate: class {
    func finishSavingData(day: Int, month: Int, year: Int);
}
