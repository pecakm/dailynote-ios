//
//  WeekViewModel.swift
//  DailyNoteIOS
//
//  Created by Mikołaj Pęcak on 18.10.2017.
//  Copyright © 2017 Mikołaj Pęcak. All rights reserved.
//

import Foundation

class WeekViewModel {
    var currentDate = Date()
    var currentComponents = DateComponents()
    var dayNames = ["Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela"];
    
    init() {
        currentDate = Calendar.current.date(byAdding: .second, value: TimeZone.current.secondsFromGMT(), to: currentDate)!
        currentComponents = Calendar.current.dateComponents([Calendar.Component.day, Calendar.Component.month, Calendar.Component.year, Calendar.Component.weekday], from: currentDate)
    }
    
    func getCurrentComponents() -> (day: Int, month: Int, year: Int) {
        if let day = currentComponents.day, let month = currentComponents.month, let year = currentComponents.year {
            return (day, month, year)
        } else {
            return (0, 0 ,0)
        }
    }
    
    func getCurrentWeekday() -> Int {
        if let day = currentComponents.weekday {
            return day
        } else {
            return 0
        }
    }
    
    func previousWeekDate() {
        currentDate = Calendar.current.date(byAdding: .day, value: -7, to: currentDate)!
        currentComponents = Calendar.current.dateComponents([Calendar.Component.day, Calendar.Component.month, Calendar.Component.year, Calendar.Component.weekday], from: currentDate)
    }
    
    func nextWeekDate() {
        currentDate = Calendar.current.date(byAdding: .day, value: 7, to: currentDate)!
        currentComponents = Calendar.current.dateComponents([Calendar.Component.day, Calendar.Component.month, Calendar.Component.year, Calendar.Component.weekday], from: currentDate)
    }
    
}
