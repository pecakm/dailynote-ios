//
//  ViewController.swift
//  DailyNoteIOS
//
//  Created by Mikołaj Pęcak on 08.10.2017.
//  Copyright © 2017 Mikołaj Pęcak. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class WeekViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DayTableCellDelegate {
    @IBOutlet weak var weekLabel: UILabel!
    @IBOutlet weak var daysTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var viewModel = WeekViewModel()
    var ref: DatabaseReference!
    let user = Auth.auth().currentUser?.uid
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        daysTableView.delegate = self
        daysTableView.dataSource = self
        let currentComponents = viewModel.getCurrentComponents()
        weekLabel.text = "\(currentComponents.day)/\(currentComponents.month)/\(currentComponents.year)"
        ref = Database.database().reference()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    // Adjust screen when keyboard appears
    @objc func adjustForKeyboard(notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            daysTableView.contentInset = UIEdgeInsets.zero
        } else {
            daysTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        daysTableView.scrollIndicatorInsets = daysTableView.contentInset
    }

    @IBAction func previousWeekClicked(_ sender: UIButton) {
        activityIndicator.startAnimating()
        viewModel.previousWeekDate()
        daysTableView.reloadData()
        let currentComponents = viewModel.getCurrentComponents()
        weekLabel.text = "\(currentComponents.day)/\(currentComponents.month)/\(currentComponents.year)"
    }
    
    @IBAction func nextWeekClicked(_ sender: UIButton) {
        activityIndicator.startAnimating()
        viewModel.nextWeekDate()
        daysTableView.reloadData()
        let currentComponents = viewModel.getCurrentComponents()
        weekLabel.text = "\(currentComponents.day)/\(currentComponents.month)/\(currentComponents.year)"
    }
    
    
    @IBAction func logout(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        do {
            try Auth.auth().signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    // Table View Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DayCell", for: indexPath) as! DayTableViewCell
        cell.delegate = self
        let currentWeekDay = viewModel.getCurrentWeekday()
        var date = Date()
        if currentWeekDay > 1 {
            date = Calendar.current.date(byAdding: .day, value: (indexPath.row + 2 - currentWeekDay), to: viewModel.currentDate)!
        } else {
            date = Calendar.current.date(byAdding: .day, value: (indexPath.row - 6), to: viewModel.currentDate)!
        }
        var components = DateComponents()
        components = Calendar.current.dateComponents([Calendar.Component.day, Calendar.Component.month, Calendar.Component.year], from: date)
        if let day = components.day {
            if let month = components.month {
                if let year = components.year {
                    cell.titleLabel.text = "\(viewModel.dayNames[indexPath.row])  \(day)/\(month)/\(year)"
                    cell.day = day
                    cell.month = month
                    cell.year = year
                    cell.user = self.user ?? "unregistered"
                    ref.child("\(self.user ?? "unregistered")/\(year)/\(month)/\(day)").observeSingleEvent(of: .value, with: { (snapshot) in
                        let value = snapshot.value as? NSDictionary
                        let text = value?["note"] as? String ?? ""
                        cell.textView.text = text
                        if indexPath.row == 6 {
                            self.activityIndicator.stopAnimating()
                        }
                    }) { (error) in
                        print(error.localizedDescription)
                    }
                }
            }
        }
        return cell
    }
    
    // Cell delegate
    func finishSavingData(day: Int, month: Int, year: Int) {
        let alert = UIAlertController(title: "\(day)/\(month)/\(year)", message: "Note saved!", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
