//
//  LoginViewController.swift
//  DailyNoteIOS
//
//  Created by Mikołaj Pęcak on 17.10.2017.
//  Copyright © 2017 Mikołaj Pęcak. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    var weekViewController: WeekViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weekViewController = WeekViewController()
        if Auth.auth().currentUser != nil {
            self.performSegue(withIdentifier: "segue", sender: self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }

    @IBAction func loginButtonClicked(_ sender: UIButton) {
        if let email = emailTextField.text {
            if let password = passwordTextField.text {
                Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                    self.performSegue(withIdentifier: "segue", sender: self)
                }
            }
        }
    }
    
    @IBAction func registerButtonClicked(_ sender: UIButton) {
        if let email = emailTextField.text {
            if let password = passwordTextField.text {
                Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
                    let alert = UIAlertController(title: user?.email, message: "Registered!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
}
